/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.asm

import com.haliksar.devel.asm.visitor.AsmVisitorImpl
import com.haliksar.devel.ast.nodes.scopes.RootScopeNode
import com.haliksar.devel.core.output.DumpPrint
import com.haliksar.devel.core.output.DumpToFile
import com.haliksar.devel.core.output.DumpToFile.Companion.DEFAULT
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.paramsreader.Param
import com.haliksar.devel.paramsreader.Params
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import java.io.FileOutputStream

class Asm(
    private val idTable: Map<String, List<Identifier>>,
    private val out: FileOutputStream
) : DumpPrint, DumpToFile, KoinComponent {

    private val visitor: AsmVisitorImpl by inject(parameters = { parametersOf(idTable) })

    fun build(tree: RootScopeNode) {
        tree.accept(visitor)
        out.write("kjkjk".toByteArray())
        out.close()
    }

    override fun printDump(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_ASM }
/*        if (param != null) {
            val visitor = get<XMLVisitorImpl>()
            tree.accept(visitor)
            visitor.printXMLTreeColored()
        }*/
    }

    override fun printDumpToFile(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_ASM_TO_FILE }
        if (param != null) {
            val path = when (val value = param.path) {
                DEFAULT -> {
                    super.printDumpToFile(params, path)
                    "${DumpToFile.DUMP_FOLDER}/asm.txt"
                }
                null -> throw NullPointerException("path is null")
                else -> value
            }

/*            File(path).printWriter().use { out ->
                val visitor = get<XMLVisitorImpl>()
                tree.accept(visitor)
                out.println(visitor.getXMLTree())
            }*/
        }
    }

}