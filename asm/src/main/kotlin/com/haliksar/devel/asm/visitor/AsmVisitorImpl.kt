/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.asm.visitor

import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.nodes.decl.*
import com.haliksar.devel.ast.nodes.expr.*
import com.haliksar.devel.ast.nodes.scopes.*
import com.haliksar.devel.ast.nodes.structures.*
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.ast.visitor.forEachVisit
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.idtable.Scope

class AsmVisitorImpl(
    private val idTable: Map<String, List<Identifier>>
) : Visitor {

    companion object {
        const val PUSH = "push"
        const val POP = "pop"
        const val BSS = ".bss"
        const val RODATA = ".rodata"
        const val SELECTION = ".section"
        const val GLOBAL = ".global"
        const val SPACE = ".space"
        const val TYPE = ".type"
        const val A_FUNCTION = "@function"
        const val TEXT = ".text"
        const val MAIN = "main"
        const val MOW = "mov"
        const val JUMP = "jmp"
        const val CMPL = "cmpl"
        const val CALL = "call"
        const val PRINTF = "printf"
        const val SCANF = "scanf"
        const val STRING = ".string"
        const val EBP = "%ebp"
        const val ESP = "%esp"
        const val EAX = "%eax"
        const val RBP = "%rbp"
        const val RAX = "%rax"
        const val RSP = "%rsp"
        const val LEAVE = "leave"
        const val RET = "ret"

        const val ADD = "add"
        const val SUB = "sub"
        const val IMUL = "imul"
        const val IDIV = "idiv"
        const val JE = "je"
        const val JL = "jl"
        const val JG = "jg"
        const val JNE = "jne"
        const val JMP = "jmp"
        const val JGE = "jge"
        const val Q = "q"

        object Offset {
            const val INT = 4
            const val BOOL = 1
            const val DOUBLE = 8
            const val VOID = 8
            const val LIST = 8
            const val STRING = 8 //????
            const val CHAR = 1 //????
        }
    }

    fun convertDouble(value: String) {

    }

    private val scope = Scope()

    var asmCode = ""
        private set

    private val asmFuncs = mutableListOf<String>()
    private val asmLC = mutableListOf<String>()
    private val rodata = mutableListOf<String>()


    private fun addRodata(vararg commands: String) {
        if (rodata.isEmpty()) {
            rodata.add("$SELECTION $RODATA")
        }
        commands.forEach { rodata.add(it) }
        "decimal_format:"
        "\t.string \"%d\""
        "decimal_format_nl:"
        "\t.string \"%d\""
        "string_format:"
        "\t.string \"%s\""
        "string_format_nl:"
        "\t.string \"%s\""
    }


    fun printAsmCode() = println(asmCode)

    private fun addFunc(
        name: String,
        scope: String
    ) {

    }

    private fun asmString(value: String) {
        val charArray = value.toCharArray()
        "movb\t${charArray.size}, -ADRESS($RBP)"
        charArray.forEach {
            "movb\t${it.toInt()}, -ADRESS($RBP)"
        }
    }

    private fun asmFunc(name: String) {
        if (name == "main") {
            "$GLOBAL $name"
            "\t$TEXT"
            "\t$TYPE $name, $A_FUNCTION"
        }
        "$name:"
        "\t${PUSH}$Q\t$RBP"
        "\t$MOW$Q\t$RSP, $RBP"

        "\t$SUB$Q\t\$ALL, $RSP"
        //gcc -ni-pie <>.s
        "\t$LEAVE"
        "\t$RET"
    }

    private fun asmFuncParams(types: List<HasType>) {
        types.forEach {
            when (it.type) {
                Token.Type.TypeString -> {
                    TODO()
                }
                Token.Type.TypeList -> {
                    TODO()
                }
                Token.Type.TypeInt -> {
                    TODO()
                }
                Token.Type.TypeBoolean -> {
                    TODO()
                }
            }
        }
    }


    override fun visit(node: ImportNode) = Unit
    override fun visit(node: ImportsNode) {
        node.getImports().forEachVisit(this)
    }

    override fun visit(node: DeclVariablesNode) {
        node.getVariables().forEachVisit(this)
    }

    override fun visit(node: DeclArraysNode) {
        node.getVariables().forEachVisit(this)
    }

    override fun visit(node: ParamsFuncNode) {
        node.getParams().forEachVisit(this)
    }

    override fun visit(node: SubjectNode) {
        node.statement?.accept(this)
    }

    override fun visit(node: DeclFuncNode) {
        node.getParams().forEach {
            (it as HasType).let {
/*                println(it.print())*/
            }
        }
        scope.add(node) {
            node.scope.accept(this)
        }
    }

    override fun visit(node: DeclVariableNode) {
        node.expression?.accept(this)
    }

    override fun visit(node: DeclArrayNode) {
        node.expression?.accept(this)
    }

    override fun visit(node: ParamFuncNode) {

    }

    override fun visit(node: ParamArrayFuncNode) {

    }

    override fun visit(node: CallFuncNode) {
        node.getParams().forEachVisit(this)
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: CallVariableNode) {
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: CallArrayNode) {
        node.expressionArray.accept(this)
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: ExprArithNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: IfNode) {
        node.tree?.accept(this)
        node.elseBlock?.let {
            scope.add(it) {
                it.accept(this)
            }
        }
    }

    override fun visit(node: ForNode) {
        scope.add(node) {
            node.expressions.forEach { it?.accept(this) }
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: WhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: DoWhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: ReturnNode) {
        node.expression?.accept(this)
    }

    override fun visit(node: ExprRelNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: ExprEqNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: FactorNode) {
        if (node.literal?.type == Token.Type.StringLiteral) {
/*            addStringConstant(node.literal?.lexeme)*/
        }
        node.expression?.accept(this)
    }

    override fun visit(node: TermNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: ArrayNode) {
        node.getElements().forEachVisit(this)
    }

    override fun visit(node: EntryExprNode) {
        node.expression.accept(this)
    }

    override fun visit(node: RootScopeNode) {
        scope.add(node) {
            node.imports?.accept(this)
            node.getFields().forEachVisit(this)


            "$SELECTION $RODATA"
            "decimal_format:"
            "\t.string \"%d\""
            "decimal_format_nl:"
            "\t.string \"%d\""
            "string_format:"
            "\t.string \"%s\""
            "string_format_nl:"
            "\t.string \"%s\""
        }
    }


    override fun visit(node: RootSubjectNode) {
        node.subject.accept(this)
    }

    override fun visit(node: ScopeFuncNode) {
        node.params?.accept(this)
        node.scope.accept(this)
    }

    override fun visit(node: ScopeNode) {
        scope.add(node) {
            node.getStatements().forEachVisit(this)
        }
    }

    override fun visit(node: ScopeFieldNode) {
        node.statement?.accept(this)
        node.returned?.accept(this)
    }

    override fun visit(node: ScopeIdentifierNode) {
        node.statement.accept(this)
    }

    override fun visit(node: ScopeExprNode) {
        node.expression.accept(this)
    }

    override fun visit(node: PrintNode) {
        node.expression.accept(this)
    }

    override fun visit(node: ScanNode) = Unit
}