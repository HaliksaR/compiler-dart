dependencies {
    implementation(project(":core"))
    implementation(project(":paramsreader"))
    implementation(project(":ast"))
    implementation(project(":idtable"))
}
