/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.semantic

import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.base.castNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.ast.visitor.forEachVisit
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.core.token.Token.Type.*

object TypeSystem {
    private val castOperationMap = mapOf(
        "$OperatorPlus$TypeInt$TypeInt" to TypeInt,
        "$OperatorPlus$TypeList$TypeList" to TypeList,
        "$OperatorPlus$TypeString$TypeString" to TypeString,

        "$OperatorMinus$TypeInt$TypeInt" to TypeInt,

        "$OperatorOr$TypeBoolean$TypeBoolean" to TypeBoolean,

        "$LessThan$TypeInt$TypeInt" to TypeBoolean,

        "$GreaterThan$TypeInt$TypeInt" to TypeBoolean,

        "$GreaterThanOrEqualTo$TypeInt$TypeInt" to TypeBoolean,

        "$LessThanOrEqualTo$TypeInt$TypeInt" to TypeBoolean,

        "$OperatorDivision$TypeInt$TypeInt" to TypeInt,

        "$OperatorMultiply$TypeInt$TypeInt" to TypeInt,

        "$OperatorAnd$TypeBoolean$TypeBoolean" to TypeBoolean,

        "$EqualTo$TypeBoolean$TypeBoolean" to TypeBoolean,
        "$EqualTo$TypeInt$TypeInt" to TypeBoolean,
        "$EqualTo$TypeString$TypeString" to TypeBoolean,

        "$NotEqualTo$TypeBoolean$TypeBoolean" to TypeBoolean,
        "$NotEqualTo$TypeInt$TypeInt" to TypeBoolean,
        "$NotEqualTo$TypeString$TypeString" to TypeBoolean,
    )


    private val possibleCastMap = mapOf(
        "$TypeInt$TypeInt" to TypeInt,
        "$TypeString$TypeString" to TypeString,
        "$TypeBoolean$TypeBoolean" to TypeBoolean,
        "$TypeList$TypeList" to TypeList,
        "$TypeChar$TypeChar" to TypeChar
    )

    fun Visitor.castTypesWithOperations(
        operations: List<Token>,
        expressions: List<BaseNode>,
        func: (Token.Type?, Token.Type?) -> Unit
    ) {
        expressions.forEachVisit(this)
        val exprs = expressions.map { it as HasType }
        val referenceType = exprs.first()
        func(referenceType.type, referenceType.elementsType)
        for (index in 1..operations.size) {
            val nextType = exprs[index]
            val operation = operations[index - 1]
            if (referenceType.type != null && nextType.type != null) {
                referenceType.type = castWithOperation(referenceType.type, nextType.type, operation.type)
                func(referenceType.type, referenceType.elementsType)
            }
        }
    }

    fun castWithOperation(type1: Token.Type?, type2: Token.Type?, operation: Token.Type): Token.Type {
        return castOperationMap["$operation${type1}${type2}"]
            ?: throw TypeCastException("Операция ${operation.name} не выполняется над типами $type1 $type2")
    }

    inline fun <reified T : HasType> checkEqExpression(node: BaseNode) {
        val type = node.castNode<T>().type
        if (type != TypeBoolean) {
            throw TypeCastException("Условие неверного типа ($type)")
        }
    }

    inline fun <reified T : HasType> checkArrayExpression(node: BaseNode) {
        val type = node.castNode<T>().type
        if (type != TypeInt) {
            throw TypeCastException("Тип ($type) вызова элемента массива должен быть int")
        }
    }

    fun possibleCast(from: Token.Type?, vararg to: Token.Type?): Token.Type {
        val casts = to.map {
            possibleCastMap["${from}${it}"] ?: throw TypeCastException("Каст типа $from в $it невозможен!")
        }
        return if (casts.all { it == from }) {
            from ?: throw TypeCastException("Начальное значение неизвестно...")
        } else {
            throw TypeCastException("Начальное значение неизвестно...")
        }
    }

    fun <T : HasType> Visitor.collectItemsType(nodes: List<BaseNode>): List<Token.Type?> =
        nodes.map {
            it.accept(this)
            it.castNode<T>().type
        }

    fun <T : HasType> Visitor.collectItemsElementsType(nodes: List<BaseNode>): List<Token.Type?> =
        nodes.map {
            it.accept(this)
            it.castNode<T>().elementsType
        }

    fun Visitor.checkParams(paramsCall: List<BaseNode>, paramsdecl: List<BaseNode>) {
        if (paramsCall.size != paramsdecl.size) {
            throw Exception("Разное количество параметров!")
        }

        paramsCall.forEachIndexed { index, it ->
            it.accept(this)
            val declParam = paramsdecl[index].castNode<HasType>()
            val paramCall = it.castNode<HasType>()
            paramCall.type = possibleCast(declParam.type, paramCall.type)
            if (paramCall.elementsType != null && declParam.elementsType != null) {
                paramCall.elementsType = possibleCast(declParam.elementsType, paramCall.elementsType)
            }
        }
    }

    fun checkTypes(node1: BaseNode, node2: BaseNode) {
        val node1Type = node1.castNode<HasType>()
        val node2Type = node2.castNode<HasType>()
        node2Type.type = possibleCast(node1Type.type, node2Type.type)
        if (node2Type.elementsType != null && node1Type.elementsType != null) {
            node2Type.elementsType = possibleCast(node1Type.elementsType, node2Type.elementsType)
        }
    }
}