/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.semantic.visitor

import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.base.castNode
import com.haliksar.devel.ast.nodes.base.castNodeNull
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.nodes.decl.*
import com.haliksar.devel.ast.nodes.expr.*
import com.haliksar.devel.ast.nodes.scopes.*
import com.haliksar.devel.ast.nodes.structures.*
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.ast.visitor.forEachVisit
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.idtable.Scope
import com.haliksar.devel.idtable.findIdentifier
import com.haliksar.devel.semantic.TypeSystem.castTypesWithOperations
import com.haliksar.devel.semantic.TypeSystem.checkArrayExpression
import com.haliksar.devel.semantic.TypeSystem.checkEqExpression
import com.haliksar.devel.semantic.TypeSystem.checkParams
import com.haliksar.devel.semantic.TypeSystem.checkTypes
import com.haliksar.devel.semantic.TypeSystem.collectItemsElementsType
import com.haliksar.devel.semantic.TypeSystem.collectItemsType
import com.haliksar.devel.semantic.TypeSystem.possibleCast
import org.jetbrains.kotlin.utils.addToStdlib.cast

class SemanticVisitorImpl(
    private val idTable: Map<String, List<Identifier>>
) : Visitor {

    private val scope = Scope()

    override fun visit(node: DeclFuncNode) {
        scope.add(node) {
            node.scope.accept(this)
            node.getReturns().forEach {
                it.type = possibleCast(it.type, node.type)
            }
        }
    }

    override fun visit(node: DeclVariableNode) {
        if (node.state == DeclarationState.NowInit) {
            node.expression?.accept(this)
            node.expression.castNodeNull<HasType>()?.let {
                it.type = possibleCast(it.type, node.type)
            }
        }
    }

    override fun visit(node: DeclVariablesNode) {
        val typeElements = collectItemsType<HasType>(node.getVariables()).filterNotNull()
        node.type = possibleCast(node.type, *typeElements.toTypedArray())
    }

    override fun visit(node: DeclArraysNode) {
        val typeElements = collectItemsType<HasType>(node.getVariables()).filterNotNull()
        node.type = possibleCast(node.type, *typeElements.toTypedArray())

        val typeElementsItem = collectItemsElementsType<HasType>(node.getVariables()).filterNotNull()
        node.elementsType = possibleCast(node.elementsType, *typeElementsItem.toTypedArray())
    }

    override fun visit(node: DeclArrayNode) {
        node.expression?.accept(this)

        if (node.elementsType == null) {
            node.elementsType = node.expression.castNodeNull<HasType>()?.type
        } else {
            node.expression.castNodeNull<HasType>()?.let {
                it.elementsType = possibleCast(it.elementsType, node.elementsType)
                it.type = possibleCast(it.type, node.type)
            }
        }
    }

    override fun visit(node: ParamsFuncNode) {
        node.getParams().forEachVisit(this)
    }

    override fun visit(node: ParamFuncNode) = Unit

    override fun visit(node: ParamArrayFuncNode) = Unit

    override fun visit(node: CallFuncNode) {
        idTable.findIdentifier<Identifier.DeclFunc>(scope.depth, node.identifier.lexeme)?.let {
            node.setType(it.node.cast())
            checkParams(node.getParams(), it.node.getParams())
            node.expression?.let {
                it.accept(this)
                checkTypes(node, it)
            }
            node.field?.accept(this)
            node.field?.let {
                if (it.identifier.lexeme == "length") {
                    if (node.type != Token.Type.TypeList && node.type != Token.Type.TypeString) {
                        println(node.type)
                        throw TypeCastException("length есть только у массивов")
                    }
                }
                node.setType(it.castNode())
            }
        }
    }

    override fun visit(node: CallVariableNode) {
        idTable.findIdentifier<Identifier.DeclVariable>(scope.depth, node.identifier.lexeme)?.let {
            node.setType(it.node.cast())
            node.expression?.let {
                it.accept(this)
                checkTypes(node, it)
            }
            node.field?.accept(this)
            node.field?.let {
                if (it.identifier.lexeme == "length") {
                    if (node.type != Token.Type.TypeList && node.type != Token.Type.TypeString) {
                        println(node.type)
                        throw TypeCastException("length есть только у массивов")
                    }
                }
                node.setType(it.castNode())
            }
            return
        }


        idTable.findIdentifier<Identifier.DeclArray>(scope.depth, node.identifier.lexeme)?.let {
            node.setType(it.node.cast())
            node.expression?.let {
                it.accept(this)
                checkTypes(node, it)
            }
            node.field?.accept(this)
            node.field?.let {
                if (it.identifier.lexeme == "length") {
                    if (node.type != Token.Type.TypeList && node.type != Token.Type.TypeString) {
                        println(node.type)
                        throw TypeCastException("length есть только у массивов")
                    }
                }
                node.setType(it.castNode())
            }
            return
        }
    }

    override fun visit(node: CallArrayNode) {
        idTable.findIdentifier<Identifier.DeclArray>(scope.depth, node.identifier.lexeme)?.let {
            node.type = it.node.elementsType
            node.elementsType = null
            node.expressionArray.accept(this)
            checkArrayExpression<HasType>(node.expressionArray)
            node.expression?.let {
                it.accept(this)
                checkTypes(node, it)
            }
            node.field?.accept(this)
            node.field?.let {
                if (it.identifier.lexeme == "length") {
                    if (node.type != Token.Type.TypeList && node.type != Token.Type.TypeString) {
                        throw TypeCastException("length есть только у массивов")
                    }
                }
                node.setType(it.castNode())
            }
        }
    }

    override fun visit(node: ReturnNode) {
        node.expression?.let {
            it.accept(this)
            node.setType(it.castNode())
        }
    }

    override fun visit(node: ArrayNode) {
        node.type = Token.Type.TypeList

        val elements = node.getElements()
        if (elements.isNotEmpty()) {
            node.getElements().first().accept(this)
            val type = elements.first().type
            val typeElements = collectItemsType<HasType>(elements).filterNotNull()
            node.elementsType =
                if (typeElements.all { it == type }) type else throw TypeCastException("Начальное значение неизвестно...")
        }
    }

    override fun visit(node: EntryExprNode) {
        node.expression.accept(this)
        node.setType(node.expression.castNode())
    }

    override fun visit(node: ScopeExprNode) {
        node.expression.accept(this)
        node.setType(node.expression.castNode())
    }

    override fun visit(node: PrintNode) {
        node.expression.accept(this)
    }

    override fun visit(node: ScanNode) = Unit

    override fun visit(node: ExprEqNode) {
        castTypesWithOperations(node.getOperations(), node.getExpressions()) { type, typeElem ->
            node.type = type
            node.elementsType = typeElem
        }
    }

    override fun visit(node: ExprRelNode) {
        castTypesWithOperations(node.getOperations(), node.getExpressions()) { type, typeElem ->
            node.type = type
            node.elementsType = typeElem
        }
    }

    override fun visit(node: ExprArithNode) {
        castTypesWithOperations(node.getOperations(), node.getExpressions()) { type, typeElem ->
            node.type = type
            node.elementsType = typeElem
        }
    }

    override fun visit(node: TermNode) {
        castTypesWithOperations(node.getOperations(), node.getExpressions()) { type, typeElem ->
            node.type = type
            node.elementsType = typeElem
        }
    }

    override fun visit(node: FactorNode) {
        node.expression?.let {
            it.accept(this)
            node.setType(it.castNode())
        }
        node.operation?.let {
            when (it.type) {
                Token.Type.OperatorMinus -> {
                    if (node.type != Token.Type.TypeInt) {
                        throw Exception("Оператор \"-\" может быть только у типов int")
                    }
                }

                Token.Type.OperatorExclamationMark -> {
                    if (node.type != Token.Type.TypeBoolean) {
                        throw Exception("Оператор ! может быть только у типов boolean")
                    }
                }
            }
        }
    }

    override fun visit(node: ScopeIdentifierNode) {
        node.statement.accept(this)
        node.setType(node.statement.castNode())
    }

    override fun visit(node: SubjectNode) {
        node.statement?.accept(this)
    }

    override fun visit(node: IfNode) {
        scope.add(node) {
            node.expression.accept(this)
            checkEqExpression<HasType>(node.expression)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
        node.tree?.accept(this)
        node.elseBlock?.let {
            scope.add(it) {
                it.accept(this)
            }
        }
    }

    override fun visit(node: ForNode) {
        scope.add(node) {
            node.expressions.forEach { it?.accept(this) }
            node.expressions[1]?.let {
                checkEqExpression<HasType>(it)
            }
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: WhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            checkEqExpression<HasType>(node.expression)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: DoWhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            checkEqExpression<HasType>(node.expression)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: RootScopeNode) {
        scope.add(node) {
            node.imports?.accept(this)
            node.getFields().forEachVisit(this)
        }
    }

    override fun visit(node: ImportsNode) {
        node.getImports().forEachVisit(this)
    }

    override fun visit(node: RootSubjectNode) {
        node.subject.accept(this)
    }

    override fun visit(node: ScopeFuncNode) {
        node.params?.accept(this)
        node.scope.accept(this)
    }

    override fun visit(node: ScopeNode) {
        scope.add(node) {
            node.getStatements().forEachVisit(this)
        }
    }

    override fun visit(node: ScopeFieldNode) {
        node.statement?.accept(this)
        node.returned?.accept(this)
    }

    override fun visit(node: ImportNode) = Unit
}