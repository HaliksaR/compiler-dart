/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.semantic

import com.haliksar.devel.ast.nodes.scopes.RootScopeNode
import com.haliksar.devel.ast.xml.XMLVisitorImpl
import com.haliksar.devel.core.output.DumpPrint
import com.haliksar.devel.core.output.DumpToFile
import com.haliksar.devel.core.output.DumpToFile.Companion.DEFAULT
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.paramsreader.Param
import com.haliksar.devel.paramsreader.Params
import com.haliksar.devel.semantic.visitor.SemanticVisitorImpl
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject
import org.koin.core.parameter.parametersOf
import java.io.File

class Semantic(
    private val idTable: Map<String, List<Identifier>>
) : DumpPrint, DumpToFile, KoinComponent {

    private val visitor: SemanticVisitorImpl by inject(parameters = { parametersOf(idTable) })

    private lateinit var tree: RootScopeNode

    fun analyze(tree: RootScopeNode) {
        this.tree = tree
        visitor.visit(tree)
    }


    override fun printDump(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_SEMA }
        if (param != null) {
            val visitor = get<XMLVisitorImpl>()
            tree.accept(visitor)
            visitor.printXMLTreeColored()
        }
    }

    override fun printDumpToFile(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_SEMA_TO_FILE }
        if (param != null) {
            val path = when (val value = param.path) {
                DEFAULT -> {
                    super.printDumpToFile(params, path)
                    "${DumpToFile.DUMP_FOLDER}/semantic_ast.txt"
                }
                null -> throw NullPointerException("path is null")
                else -> value
            }

            File(path).printWriter().use { out ->
                val visitor = get<XMLVisitorImpl>()
                tree.accept(visitor)
                out.println(visitor.xmlTree)
            }
        }
    }
}