/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.expr

import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class TermNode : ExprBaseNode() {

    override var type: Token.Type? = null
    override var elementsType: Token.Type? = null

    override fun parse(): Boolean {
        parseWrapper(FactorNode())?.let {
            exprs.add(it)
        } ?: errorWrapper<TermNode>(message = "При парсинге FactorNode произошла ошибка")

        while (true) {
            when (tm.peekType()) {
                Token.Type.OperatorMultiply,
                Token.Type.OperatorDivision,
                Token.Type.OperatorAnd -> {
                    ops.add(tm.peek())
                    skipThrow<TermNode>(tm.peekType(), message = "Произошло чудо!")
                    parseWrapper(FactorNode())?.let {
                        exprs.add(it)
                    } ?: errorWrapper<TermNode>(message = "При парсинге FactorNode произошла ошибка")
                }
                else -> return true
            }
        }
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}
