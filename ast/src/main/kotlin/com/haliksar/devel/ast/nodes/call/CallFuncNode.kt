/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.call

import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.nodes.scopes.ScopeIdentifierNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class CallFuncNode(
    override var identifier: Token,
) : BaseNode(), IsCallable {

    var operation: Token? = null
        private set

    private val params = mutableListOf<EntryExprNode>()
    fun getParams() = params.toList()

    override var type: Token.Type? = null
    override var elementsType: Token.Type? = null

    override lateinit var state: CallState

    override var expression: BaseNode? = null

    override var field: ScopeIdentifierNode? = null
    override var isMutable: Boolean = false

    override fun parse(): Boolean {
        skipThrow<CallFuncNode>(Token.Type.LParen, message = "Ожидался символ (")
        while (tm.peekType() != Token.Type.RParen) {
            parseWrapper(EntryExprNode())?.let {
                params.add(it)
            } ?: errorWrapper<CallFuncNode>(message = "При парсинге EntryExprNode произошла ошибка")
            skipIf<CallFuncNode>(condition = tm.peekType() == Token.Type.Comma, message = "Ожидался символ ,")
        }
        skipThrow<CallFuncNode>(Token.Type.RParen, message = "Ожидался символ )")


        state = findState()
        when (state) {
            CallState.CallField -> {
                parseWrapper(ScopeIdentifierNode())?.let {
                    field = it
                } ?: errorWrapper<CallFuncNode>(message = "При парсинге ScopeIdentifierNode произошла ошибка")
            }

            CallState.Operation -> {
                errorWrapper<CallFuncNode>(message = "Assign операции для вызова функции недоступны")
            }
            CallState.Call -> { /*Skip*/
            }
        }
        return true
    }

    private fun findState(): CallState = when (tm.peekType()) {
        Token.Type.OperatorAssignment -> {
            operation = tm.peek()
            skipThrow<CallFuncNode>(tm.peekType(), message = "Произошло чудо!")
            CallState.Operation
        }

        Token.Type.Dot -> {
            skipThrow<CallFuncNode>(Token.Type.Dot, message = "Ожидался символ .")
            CallState.CallField
        }

        else -> CallState.Call
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)

    override fun toString() = "CALL FUNC $state ${identifier.lexeme} $type"
}