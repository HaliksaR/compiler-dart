/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes

import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.decl.DeclArraysNode
import com.haliksar.devel.ast.nodes.decl.DeclFuncNode
import com.haliksar.devel.ast.nodes.decl.DeclVariablesNode
import com.haliksar.devel.ast.nodes.decl.ParamArrayFuncNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class SubjectNode : BaseNode() {

    enum class TypeSubject {
        Function,
        Variables,
        Arrays,
        Constant,
        ConstantArray
    }

    private var type: Token.Type? = null
    private var elementType: Token.Type? = null

    private lateinit var identifier: Token

    lateinit var state: TypeSubject
        private set

    var statement: BaseNode? = null
        private set

    override fun parse(): Boolean {

        when (tm.peekType()) {
            Token.Type.TypeInt,
            Token.Type.TypeBoolean,
            Token.Type.TypeChar -> {
                type = tm.peek().type
                skipThrow<SubjectNode>(tm.peek().type, message = "Произошло чудо!")
            }

            Token.Type.TypeString -> {
                type = Token.Type.TypeString
                elementType = Token.Type.TypeChar
                skipThrow<SubjectNode>(Token.Type.TypeString, message = "Ожидался символ String")
            }

            Token.Type.TypeList -> {
                type = Token.Type.TypeList
                skipThrow<SubjectNode>(Token.Type.TypeList, message = "Ожидался символ List")
                if (tm.peekType() == Token.Type.LessThan) {
                    skipThrow<ParamArrayFuncNode>(Token.Type.LessThan, message = "Ожидался <")
                    elementType = tm.get().type
                    skipThrow<ParamArrayFuncNode>(Token.Type.GreaterThan, message = "Ожидался >")
                }
            }

            else -> errorWrapper<SubjectNode>(message = "Ожидался тип")
        }

        when (tm.peekType()) {
            Token.Type.Identifier -> {
                identifier = tm.peek()
                skipThrow<SubjectNode>(tm.peekType(), message = "Произошло чудо!")
            }

            Token.Type.Semicolon,
            Token.Type.LessThan -> {
                /* Skip */
            }

            else -> errorWrapper<SubjectNode>(message = "Ожидался тип или индентификатор")
        }

        when (tm.peekType()) {
            Token.Type.OperatorAssignment,
            Token.Type.Semicolon,
            Token.Type.Comma,
            Token.Type.LessThan -> {
                when {
                    type == Token.Type.TypeList || type == Token.Type.TypeString -> {
                        state = TypeSubject.Arrays
                        parseWrapper(DeclArraysNode(identifier, type, elementType))?.let {
                            statement = it
                        } ?: errorWrapper<SubjectNode>(message = "При парсинге DeclArraysNode произошла ошибка")
                    }
                    type != Token.Type.TypeList || type != Token.Type.TypeString -> {
                        state = TypeSubject.Variables
                        parseWrapper(DeclVariablesNode(identifier, type, elementType))?.let {
                            statement = it
                        } ?: errorWrapper<SubjectNode>(message = "При парсинге DeclVariablesNode произошла ошибка")
                    }
                }
            }

            Token.Type.LParen -> {
                state = TypeSubject.Function
                parseWrapper(DeclFuncNode(identifier, type, elementType))?.let {
                    statement = it
                } ?: errorWrapper<SubjectNode>(message = "При парсинге DeclFuncNode произошла ошибка")
            }

            else -> errorWrapper<SubjectNode>(message = "Ожидался символ = ; , (")
        }
        skipIf<SubjectNode>(Token.Type.Semicolon, state != TypeSubject.Function, message = "Ожидался символ ;")
        return true
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}