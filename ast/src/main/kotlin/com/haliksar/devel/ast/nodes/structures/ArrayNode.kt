/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.structures

import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class ArrayNode : BaseNode(), HasType {

    override var type: Token.Type? = Token.Type.TypeList
    override var elementsType: Token.Type? = null

    private val elements = mutableListOf<EntryExprNode>()
    fun getElements() = elements.toList()

    override fun parse(): Boolean {
        skipThrow<ArrayNode>(Token.Type.LBox, message = "Ожидался символ [")
        while (tm.peekType() != Token.Type.RBox) {
            skipIf<ArrayNode>(condition = tm.peekType() == Token.Type.Comma, message = "Ожидался символ (")
            parseWrapper(EntryExprNode())?.let {
                elements.add(it)
            } ?: errorWrapper<ArrayNode>(message = "При парсинге EntryExprNode произошла ошибка")
        }
        skipThrow<ArrayNode>(Token.Type.RBox, message = "Ожидался символ ]")
        return true
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}