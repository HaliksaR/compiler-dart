/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.structures

import com.haliksar.devel.ast.ext.HasScope
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.nodes.scopes.ScopeNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class ForNode(
    private val refReturns: MutableList<ReturnNode>
) : BaseNode(), HasScope {

    var expressions = arrayOfNulls<BaseNode?>(3)
        private set

    lateinit var block: ScopeNode
        private set

    override fun parse(): Boolean {
        skipThrow<ForNode>(Token.Type.KeywordFor, Token.Type.LParen, message = "Ожидался символ for(")
        parseWrapper(SubjectNode())?.let {
            expressions[0] = it
        } ?: errorWrapper<ForNode>(message = "При парсинге SubjectNode произошла ошибка")

        if (tm.peekType() != Token.Type.Semicolon) {
            parseWrapper(EntryExprNode())?.let {
                expressions[1] = it
            } ?: errorWrapper<ForNode>(message = "При парсинге EntryExprNode произошла ошибка")
        }
        skipThrow<ForNode>(Token.Type.Semicolon, message = "Ожидался символ ;")
        if (tm.peekType() != Token.Type.Semicolon) {
            parseWrapper(EntryExprNode())?.let {
                expressions[2] = it
            } ?: errorWrapper<ForNode>(message = "При парсинге EntryExprNode произошла ошибка")
        }
        skipThrow<ForNode>(Token.Type.RParen, message = "Ожидался символ (")
        parseWrapper(ScopeNode(refReturns))?.let {
            block = it
        } ?: errorWrapper<ForNode>(message = "При парсинге ScopeNode произошла ошибка")
        return true
    }

    override fun scopeName() = "for"

    override fun accept(visitor: Visitor) = visitor.visit(this)

}