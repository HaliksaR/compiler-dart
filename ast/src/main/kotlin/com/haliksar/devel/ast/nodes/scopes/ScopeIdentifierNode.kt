/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.scopes

import com.haliksar.devel.ast.ext.HasIdentifier
import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class ScopeIdentifierNode : BaseNode(), HasIdentifier, HasType {
    enum class State {
        CallFunc,
        CallVariable,
        CallArray
    }

    override lateinit var identifier: Token

    lateinit var state: State
        private set

    lateinit var statement: BaseNode
        private set

    override var type: Token.Type? = null
    override var elementsType: Token.Type? = null

    override fun parse(): Boolean {
        when (tm.peekType()) {
            Token.Type.Identifier -> {
                identifier = tm.peek()
                skipThrow<ScopeIdentifierNode>(Token.Type.Identifier, message = "Ожидался идентификатор")
            }
            else -> errorWrapper<ScopeIdentifierNode>(message = "Ожидался идентификатор")
        }
        state = state()
        when (state) {
            State.CallFunc -> parseWrapper(CallFuncNode(identifier))?.let {
                statement = it
            } ?: errorWrapper<ScopeIdentifierNode>(message = "При парсинге CallFuncNode произошла ошибка")

            State.CallVariable -> parseWrapper(CallVariableNode(identifier))?.let {
                statement = it
            } ?: errorWrapper<ScopeIdentifierNode>(message = "При парсинге CallVariableNode произошла ошибка")

            State.CallArray -> parseWrapper(CallArrayNode(identifier))?.let {
                statement = it
            } ?: errorWrapper<ScopeIdentifierNode>(message = "При парсинге CallArrayNode произошла ошибка")
        }
        return true
    }

    private fun state() =
        when (tm.peekType()) {
            Token.Type.OperatorAssignment,
            Token.Type.LessThan,
            Token.Type.GreaterThan,
            Token.Type.GreaterThanOrEqualTo,
            Token.Type.LessThanOrEqualTo,
            Token.Type.EqualTo,
            Token.Type.NotEqualTo,
            Token.Type.OperatorMinus,
            Token.Type.Dot,
            Token.Type.Semicolon,
            Token.Type.RParen,
            Token.Type.RBox -> State.CallVariable

            Token.Type.LBox -> State.CallArray

            Token.Type.LParen -> State.CallFunc

            else -> State.CallVariable
        }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}