/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.xml

import com.github.ajalt.mordant.AnsiCode
import com.github.ajalt.mordant.TermColors
import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.nodes.decl.*
import com.haliksar.devel.ast.nodes.expr.*
import com.haliksar.devel.ast.nodes.scopes.*
import com.haliksar.devel.ast.nodes.structures.*
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.ast.visitor.forEachVisit
import org.koin.core.KoinComponent
import org.koin.core.inject

class XMLVisitorImpl : Visitor, KoinComponent {

    private var nesting = 0L

    private val tabs: () -> String = {
        var string = ""
        for (i in 0 until nesting) string += "  "
        string
    }

    private val term by inject<TermColors>()

    var xmlTreeColored = ""
        private set

    var xmlTree = ""
        private set

    fun printXMLTreeColored() = println(xmlTreeColored)

    fun printXMLTree() = println(xmlTree)

    private inline fun <reified Node : BaseNode> xmlPatternBody(
        noinline vars: (() -> Unit)? = null,
        noinline block: (() -> Unit)? = null,
        color: AnsiCode = term.reset
    ) {
        val name = Node::class.simpleName?.removeSuffix("Node")
        "${tabs()}<$name".also {
            xmlTreeColored += color(it)
            xmlTree += it
        }
        vars?.invoke()
        block?.let {
            ">".also {
                xmlTreeColored += "${color(it)}\n"
                xmlTree += "$it\n"
            }
            nesting++
            it()
            nesting--
            "${tabs()}<\\$name>".also {
                xmlTreeColored += "${color(it)}\n"
                xmlTree += "$it\n"
            }
        } ?: ">".also {
            xmlTreeColored += "${color(it)}\n"
            xmlTree += "$it\n"
        }

    }

    private fun xmlPatternVar(color: AnsiCode = term.reset, name: String, value: Any?) {
        value?.let {
            xmlTreeColored += " $name=\"${color(it.toString())}\""
            xmlTree += " $name=${"\"$it\""}"
        }
    }

    override fun visit(node: ImportNode) =
        xmlPatternBody<ImportNode>(
            vars = {
                xmlPatternVar(
                    color = term.blue,
                    name = "lib",
                    value = node.lib.lexeme
                )
            }
        )

    override fun visit(node: SubjectNode) =
        xmlPatternBody<SubjectNode>(
            vars = {
                xmlPatternVar(
                    color = term.magenta,
                    name = "state",
                    value = node.state
                )
            },
            block = {
                node.statement?.accept(this)
            }
        )

    override fun visit(node: DeclFuncNode) =
        xmlPatternBody<DeclFuncNode>(
            color = term.cyan,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "countParams",
                    value = node.getParams().size
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.scope.accept(this)
            }
        )

    override fun visit(node: DeclVariableNode) =
        xmlPatternBody<DeclVariableNode>(
            color = term.cyan,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.yellow,
                    name = "operation",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression?.accept(this)
            }
        )

    override fun visit(node: DeclVariablesNode) =
        xmlPatternBody<DeclVariablesNode>(
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getVariables().forEachVisit(this)
            }
        )

    override fun visit(node: DeclArraysNode) =
        xmlPatternBody<DeclArraysNode>(
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getVariables().forEachVisit(this)
            }
        )

    override fun visit(node: DeclArrayNode) =
        xmlPatternBody<DeclArrayNode>(
            color = term.magenta,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.yellow,
                    name = "operation",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression?.accept(this)
            }
        )

    override fun visit(node: ParamsFuncNode) =
        xmlPatternBody<ParamsFuncNode>(
            block = {
                node.getParams().forEachVisit(this)
            }
        )

    override fun visit(node: ParamFuncNode) =
        xmlPatternBody<ParamFuncNode>(
            color = term.yellow,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            }
        )

    override fun visit(node: ParamArrayFuncNode) =
        xmlPatternBody<ParamArrayFuncNode>(
            color = term.magenta,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            }
        )

    override fun visit(node: CallFuncNode) =
        xmlPatternBody<CallFuncNode>(
            color = term.cyan,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.green,
                    name = "operation",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "countParams",
                    value = node.getParams().size
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getParams().forEachVisit(this)
                node.expression?.accept(this)
                node.field?.accept(this)
            }
        )

    override fun visit(node: CallVariableNode) =
        xmlPatternBody<CallVariableNode>(
            color = term.yellow,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.green,
                    name = "operation",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression?.accept(this)
                node.field?.accept(this)
            }
        )

    override fun visit(node: CallArrayNode) =
        xmlPatternBody<CallArrayNode>(
            color = term.yellow,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.green,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.green,
                    name = "operation",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expressionArray.accept(this)
                node.expression?.accept(this)
                node.field?.accept(this)
            }
        )

    override fun visit(node: ExprArithNode) =
        xmlPatternBody<ExprArithNode>(
            vars = {
                node.getOperations().forEach {
                    xmlPatternVar(
                        color = term.green,
                        name = "op",
                        value = it.lexeme
                    )
                }
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getExpressions().forEachVisit(this)
            }
        )


    override fun visit(node: IfNode) =
        xmlPatternBody<IfNode>(
            color = term.yellow,
            block = {
                node.expression.accept(this)
                node.block.accept(this)
                node.tree?.accept(this)
                node.elseBlock?.accept(this)
            }
        )

    override fun visit(node: ForNode) =
        xmlPatternBody<ForNode>(
            color = term.yellow,
            block = {
                node.expressions.forEach { it?.accept(this) }
                node.block.accept(this)
            }
        )

    override fun visit(node: WhileNode) =
        xmlPatternBody<WhileNode>(
            color = term.yellow,
            block = {
                node.expression.accept(this)
                node.block.accept(this)
            }
        )

    override fun visit(node: DoWhileNode) =
        xmlPatternBody<DoWhileNode>(
            color = term.yellow,
            block = {
                node.expression.accept(this)
                node.block.accept(this)
            }
        )

    override fun visit(node: ReturnNode) =
        xmlPatternBody<ReturnNode>(
            color = term.yellow,
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression?.accept(this)
            }
        )

    override fun visit(node: ExprRelNode) =
        xmlPatternBody<ExprRelNode>(
            vars = {
                node.getOperations().forEach {
                    xmlPatternVar(
                        color = term.green,
                        name = "op",
                        value = it.lexeme
                    )
                }
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getExpressions().forEachVisit(this)
            }
        )

    override fun visit(node: ExprEqNode) =
        xmlPatternBody<ExprRelNode>(
            vars = {
                node.getOperations().forEach {
                    xmlPatternVar(
                        color = term.green,
                        name = "op",
                        value = it.lexeme
                    )
                }
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getExpressions().forEachVisit(this)
            }
        )

    override fun visit(node: FactorNode) =
        xmlPatternBody<FactorNode>(
            color = term.cyan,
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "literal",
                    value = node.literal?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "op",
                    value = node.operation?.lexeme
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression?.accept(this)
            }
        )

    override fun visit(node: TermNode) =
        xmlPatternBody<TermNode>(
            vars = {
                node.getOperations().forEach {
                    xmlPatternVar(
                        color = term.green,
                        name = "op",
                        value = it.lexeme
                    )
                }
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getExpressions().forEachVisit(this)
            }
        )


    override fun visit(node: ArrayNode) =
        xmlPatternBody<ArrayNode>(
            vars = {
                xmlPatternVar(
                    color = term.green,
                    name = "countElement",
                    value = node.getElements().size
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.getElements().forEachVisit(this)
            }
        )

    override fun visit(node: EntryExprNode) =
        xmlPatternBody<EntryExprNode>(
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression.accept(this)
            }
        )

    override fun visit(node: RootScopeNode) =
        xmlPatternBody<RootScopeNode>(
            color = term.red,
            block = {
                node.imports?.accept(this)
                node.getFields().forEachVisit(this)
            }
        )

    override fun visit(node: ImportsNode) =
        xmlPatternBody<ImportsNode>(
            color = term.green,
            block = {
                node.getImports().forEachVisit(this)
            }
        )

    override fun visit(node: RootSubjectNode) =
        xmlPatternBody<RootSubjectNode>(
            block = {
                node.subject.accept(this)
            }
        )

    override fun visit(node: ScopeFuncNode) =
        xmlPatternBody<ScopeFuncNode>(
            block = {
                node.params?.accept(this)
                node.scope.accept(this)
            }
        )

    override fun visit(node: ScopeNode) =
        xmlPatternBody<ScopeNode>(
            block = {
                node.getStatements().forEachVisit(this)
            }
        )

    override fun visit(node: ScopeFieldNode) =
        xmlPatternBody<ScopeFieldNode>(
            block = {
                node.statement?.accept(this)
                node.returned?.accept(this)
            }
        )

    override fun visit(node: ScopeIdentifierNode) =
        xmlPatternBody<ScopeIdentifierNode>(
            vars = {
                xmlPatternVar(
                    color = term.magenta,
                    name = "id",
                    value = node.identifier.lexeme
                )
                xmlPatternVar(
                    color = term.magenta,
                    name = "state",
                    value = node.state
                )
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.statement.accept(this)
            }
        )

    override fun visit(node: ScopeExprNode) =
        xmlPatternBody<ScopeExprNode>(
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression.accept(this)
            }
        )

    override fun visit(node: PrintNode) =
        xmlPatternBody<PrintNode>(
            color = term.blue,
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            },
            block = {
                node.expression.accept(this)
            }
        )

    override fun visit(node: ScanNode) =
        xmlPatternBody<ScanNode>(
            color = term.blue,
            vars = {
                xmlPatternVar(
                    color = term.red,
                    name = "type",
                    value = node.type
                )
                xmlPatternVar(
                    color = term.red,
                    name = "elementsType",
                    value = node.elementsType
                )
            }
        )
}