/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.visitor

import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.nodes.decl.*
import com.haliksar.devel.ast.nodes.expr.*
import com.haliksar.devel.ast.nodes.scopes.*
import com.haliksar.devel.ast.nodes.structures.*

interface Visitor {
    fun visit(node: ImportNode)
    fun visit(node: SubjectNode)
    fun visit(node: DeclFuncNode)
    fun visit(node: DeclVariableNode)
    fun visit(node: DeclVariablesNode)
    fun visit(node: DeclArraysNode)
    fun visit(node: DeclArrayNode)
    fun visit(node: ParamsFuncNode)
    fun visit(node: ParamFuncNode)
    fun visit(node: ParamArrayFuncNode)
    fun visit(node: CallFuncNode)
    fun visit(node: CallVariableNode)
    fun visit(node: CallArrayNode)
    fun visit(node: ExprArithNode)
    fun visit(node: IfNode)
    fun visit(node: ForNode)
    fun visit(node: WhileNode)
    fun visit(node: DoWhileNode)
    fun visit(node: ReturnNode)
    fun visit(node: ExprRelNode)
    fun visit(node: ExprEqNode)
    fun visit(node: FactorNode)
    fun visit(node: TermNode)
    fun visit(node: ArrayNode)
    fun visit(node: EntryExprNode)

    fun visit(node: RootScopeNode)
    fun visit(node: ImportsNode)
    fun visit(node: RootSubjectNode)
    fun visit(node: ScopeFuncNode)
    fun visit(node: ScopeNode)
    fun visit(node: ScopeFieldNode)
    fun visit(node: ScopeIdentifierNode)
    fun visit(node: ScopeExprNode)
    fun visit(node: PrintNode)
    fun visit(node: ScanNode)
}

fun List<BaseNode>.forEachVisit(visitor: Visitor) {
    this.forEach { it.accept(visitor) }
}