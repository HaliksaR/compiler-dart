/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.base

import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.core.token.TokenManager
import org.koin.core.KoinComponent
import org.koin.core.inject

abstract class BaseNode : KoinComponent {
    abstract fun accept(visitor: Visitor)
    abstract fun parse(): Boolean

    protected val tm: TokenManager by inject()

    protected inline fun <reified Node : BaseNode> skipThrow(vararg type: Token.Type, message: String? = null) {
        type.forEach {
            val token = tm.get()
            if (token.type != it) {
                throw Exception(
                    "Skip ${tm.peek().lexeme}:${tm.peek().positions} error in ${Node::class.simpleName}" +
                            " ${message?.let { "with message: $message" } ?: ""}")
            }

        }
    }

    protected inline fun <reified Node : BaseNode> skipIf(
        type: Token.Type? = null,
        condition: Boolean,
        message: String
    ): Token? {
        if (condition) {
            val token = tm.get()
            type?.let {
                if (token.type == it) return token else errorWrapper<Node>(message = message)
            }
        }
        return null
    }

    protected fun <Node : BaseNode> parseWrapper(node: Node) = if (node.parse()) node else null

    protected inline fun <reified Node : BaseNode> errorWrapper(
        condition: Boolean? = null,
        message: String? = null
    ): Boolean {
        if (condition == false) {
            return true
        }
        throw Exception(
            "Error parse ${tm.peek().lexeme}:${tm.peek().positions} in ${Node::class.simpleName}" +
                    " ${message?.let { "with message: $it" } ?: ""}"
        )
    }

}


@Suppress("UNCHECKED_CAST")
fun <T> BaseNode.castNode(): T = this as? T ?: throw TypeCastException("castNode $this Error")

@Suppress("UNCHECKED_CAST")
fun <T> BaseNode?.castNodeNull(): T? = this as? T
