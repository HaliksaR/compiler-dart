/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.scopes

import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.nodes.structures.*
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class ScopeFieldNode(
    private val refReturns: MutableList<ReturnNode> = mutableListOf()
) : BaseNode() {

    var statement: BaseNode? = null
        private set

    var returned: ReturnNode? = null
        private set

    override fun parse(): Boolean {
        when (tm.peekType()) {
            Token.Type.Identifier ->
                parseWrapper(ScopeIdentifierNode())?.let {
                    statement = it
                    skipThrow<ScopeFieldNode>(Token.Type.Semicolon, message = "Ожидался символ ;")
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге ScopeIdentifierNode произошла ошибка")

            Token.Type.KeywordIf ->
                parseWrapper(IfNode(refReturns))?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге IfNode произошла ошибка")

            Token.Type.KeywordFor ->
                parseWrapper(ForNode(refReturns))?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге ForNode произошла ошибка")

            Token.Type.KeywordReturn ->
                parseWrapper(ReturnNode())?.let {
                    returned = it
                    refReturns.add(it)
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге ReturnNode произошла ошибка")

            Token.Type.KeywordWhile ->
                parseWrapper(WhileNode(refReturns))?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге WhileNode произошла ошибка")

            Token.Type.KeywordDoWhile ->
                parseWrapper(DoWhileNode(refReturns))?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге DoWhileNode произошла ошибка")

            Token.Type.LFigured ->
                parseWrapper(ScopeNode(refReturns))?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге ScopeNode произошла ошибка")

            Token.Type.LParen,
            Token.Type.OperatorMinus,
            Token.Type.OperatorExclamationMark,
            Token.Type.StringLiteral,
            Token.Type.IntLiteral,
            Token.Type.BooleanLiteralTrue,
            Token.Type.BooleanLiteralFalse,
            Token.Type.LBox ->
                parseWrapper(EntryExprNode())?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге EntryExprNode произошла ошибка")

            Token.Type.TypeString,
            Token.Type.TypeList,
            Token.Type.TypeInt,
            Token.Type.TypeBoolean,
            Token.Type.TypeChar ->
                parseWrapper(SubjectNode())?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге SubjectNode произошла ошибка")

            Token.Type.Semicolon -> {
                skipThrow<ScopeFieldNode>(Token.Type.Semicolon, message = "Ожидался символ ;")
            }

            Token.Type.KeywordPrint ->
                parseWrapper(PrintNode())?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге PrintNode произошла ошибка")

            Token.Type.KeywordScan ->
                parseWrapper(ScanNode())?.let {
                    statement = it
                } ?: errorWrapper<ScopeFieldNode>(message = "При парсинге ScanNode произошла ошибка")

            else -> errorWrapper<ScopeFieldNode>(message = "Неизвестная конструкция")
        }

        return true
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}