/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.scopes

import com.haliksar.devel.ast.ext.HasScope
import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.structures.ImportsNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class RootScopeNode : BaseNode(), HasScope {

    var imports: ImportsNode? = null
        private set

    private val fields = mutableListOf<RootSubjectNode>()
    fun getFields() = fields.toList()

    override fun parse(): Boolean {
        if (tm.peekType() == Token.Type.KeywordImport) {
            parseWrapper(ImportsNode())?.let {
                imports = it
            } ?: errorWrapper<RootScopeNode>(message = "При парсинге ImportsNode произошла ошибка")
        }
        while (tm.peekType() != Token.Type.End) {
            parseWrapper(RootSubjectNode())?.let {
                fields.add(it)
            } ?: errorWrapper<RootScopeNode>(message = "При парсинге RootSubjectNode произошла ошибка")
        }
        return true
    }

    override fun scopeName() = "root"

    override fun accept(visitor: Visitor) = visitor.visit(this)
}