/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.structures

import com.haliksar.devel.ast.ext.HasScope
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.nodes.scopes.ScopeNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class DoWhileNode(
    private val refReturns: MutableList<ReturnNode>
) : BaseNode(), HasScope {

    lateinit var expression: EntryExprNode
        private set

    lateinit var block: ScopeNode
        private set

    override fun parse(): Boolean {
        skipThrow<DoWhileNode>(Token.Type.KeywordDoWhile, message = "Ожидался символ do")
        parseWrapper(ScopeNode(refReturns))?.let {
            block = it
        } ?: errorWrapper<DoWhileNode>(message = "При парсинге ScopeNode произошла ошибка")
        skipThrow<DoWhileNode>(Token.Type.KeywordWhile, Token.Type.LParen, message = "Ожидался символ while(")
        parseWrapper(EntryExprNode())?.let {
            expression = it
        } ?: return errorWrapper<DoWhileNode>(message = "При парсинге ScopeExprNode произошла ошибка")
        skipThrow<DoWhileNode>(Token.Type.RParen, Token.Type.Semicolon, message = "Ожидался символ );")
        return true
    }

    override fun scopeName() = "do_while"

    override fun accept(visitor: Visitor) = visitor.visit(this)
}