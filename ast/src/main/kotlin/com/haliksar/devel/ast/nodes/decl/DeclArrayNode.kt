/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.decl

import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.expr.EntryExprNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class DeclArrayNode(
    override var identifier: Token,
    override var type: Token.Type?,
    override var elementsType: Token.Type?,
) : BaseNode(), IsDeclaration {

    var expression: BaseNode? = null
        private set

    lateinit var state: DeclarationState
        private set

    var operation: Token? = null
        private set

    override fun parse(): Boolean {
        when (tm.peekType()) {
            Token.Type.OperatorAssignment -> {
                operation = tm.peek()
                skipThrow<DeclArrayNode>(Token.Type.OperatorAssignment, message = "Ожидался символ =")
                parseWrapper(EntryExprNode())?.let {
                    expression = it
                } ?: errorWrapper<DeclArrayNode>(message = "При парсинге EntryExprNode произошла ошибка")
                state = DeclarationState.NowInit
            }

            Token.Type.Semicolon,
            Token.Type.Comma -> state = DeclarationState.LateInit

            else -> errorWrapper<DeclArrayNode>(message = "При парсинге DeclArrayNode произошла ошибка")
        }
        return true
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)

    override fun toString() = "DECL ARRAY $type $elementsType : ${identifier.lexeme}"
}