/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.ast.nodes.expr

import com.haliksar.devel.ast.ext.HasType
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.nodes.scopes.ScopeIdentifierNode
import com.haliksar.devel.ast.nodes.structures.ArrayNode
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.core.token.Token

class FactorNode : BaseNode(), HasType {

    var literal: Token? = null
        private set

    var expression: BaseNode? = null
        private set

    var operation: Token? = null
        private set

    override var type: Token.Type? = null
    override var elementsType: Token.Type? = null

    override fun parse(): Boolean {
        when (tm.peekType()) {
            Token.Type.Identifier
            -> parseWrapper(ScopeIdentifierNode())?.let {
                expression = it
            } ?: errorWrapper<FactorNode>(message = "При парсинге ScopeIdentifierNode произошла ошибка")

            Token.Type.LParen -> {
                skipThrow<FactorNode>(Token.Type.LParen, message = "Ожидался символ (")
                parseWrapper(ScopeExprNode())?.let {
                    expression = it
                } ?: errorWrapper<FactorNode>(message = "При парсинге ScopeExprNode произошла ошибка")
                skipThrow<FactorNode>(Token.Type.RParen, message = "Ожидался символ )")
            }

            Token.Type.OperatorMinus,
            Token.Type.OperatorExclamationMark -> {
                operation = tm.peek()
                skipThrow<FactorNode>(tm.peekType(), message = "Произошло чудо!")
                parseWrapper(FactorNode())?.let {
                    expression = it
                } ?: errorWrapper<FactorNode>(message = "При парсинге ExprNode произошла ошибка")
            }

            Token.Type.StringLiteral -> {
                type = Token.Type.TypeString
                elementsType = Token.Type.TypeChar
                literal = tm.peek()
                skipThrow<FactorNode>(Token.Type.StringLiteral, message = "Ожидался строчный литерал")
            }

            Token.Type.IntLiteral -> {
                type = Token.Type.TypeInt
                literal = tm.peek()
                skipThrow<FactorNode>(Token.Type.IntLiteral, message = "Ожидался численный литерал")
            }

            Token.Type.BooleanLiteralTrue,
            Token.Type.BooleanLiteralFalse -> {
                type = Token.Type.TypeBoolean
                literal = tm.peek()
                skipThrow<FactorNode>(tm.peekType(), message = "Ожидался логический литерал")
            }

            Token.Type.LBox -> parseWrapper(ArrayNode())?.let {
                expression = it
            } ?: errorWrapper<FactorNode>(message = "При парсинге ArrayNode произошла ошибка")

            Token.Type.Semicolon -> {
                /*Skip*/
            }

            else -> errorWrapper<FactorNode>(message = "Неизвестная операция")
        }
        return true
    }

    override fun accept(visitor: Visitor) = visitor.visit(this)
}