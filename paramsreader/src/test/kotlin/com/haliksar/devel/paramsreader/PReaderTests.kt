/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.paramsreader

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.koin.KoinListener
import io.kotest.matchers.shouldBe
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.get

class PReaderTests : StringSpec(), KoinTest {
    private val agrsWithPath = arrayOf(
        "--dump-tokens", "-dt=nc",
        "--dump-ast",
        "--dump-id-table=to-file",
        "-dit=tf",
        "-dasm",
        "/src/main/resources/notDeclarationOtherScope.dart"
    )
    private val agrsWithoutPath = arrayOf(
        "--dump-tokens", "-dt=nc",
        "--dump-ast",
        "-dasm"
    )

    private val agrsPathInvalid = arrayOf(
        "--dump-tokens", "-dt=nc",
        "--dump-ast",
        "-dasm",
        "\\src\\main\\resourcesDevTest.dart"
    )

    private val agrsInvalid = arrayOf(
        "--dump-tokens", "-dt=nc",
        "--dum=warnings",
        "--dump-sco=all"
    )

    private val agrsEmpty = arrayOf<String>()

    private val module = module {
        factory { (args: Array<String>) -> PReader(args) }
    }

    override fun listeners() = listOf(KoinListener(listOf(module)))

    private fun getPReader(args: Array<String>) = get<PReader>(parameters = { parametersOf(args) })

    init {
        "Аргументы лексера" {
            val pr = getPReader(agrsWithPath)
            pr.parse()
            pr.getParamsLexer().size shouldBe 2
        }
        "Агрументы таблицы символов" {
            val pr = getPReader(agrsWithPath)
            pr.parse()
            pr.getParamsTable().size shouldBe 2
        }
        "Агрументы АСТ дерева" {
            val pr = getPReader(agrsWithPath)
            pr.parse()
            pr.getParamsAST().size shouldBe 1
        }
        "Аргументы Асеммблера" {
            val pr = getPReader(agrsWithPath)
            pr.parse()
            pr.getParamsASM().size shouldBe 1
        }
        "Без пути к файлу" {
            val pr = getPReader(agrsWithoutPath)
            shouldThrow<PReader.PReaderException> {
                pr.parse()
            }
        }
        "Некорректный аргумент" {
            val pr = getPReader(agrsInvalid)
            shouldThrow<PReader.PReaderException> {
                pr.parse()
            }
            val pr2 = getPReader(agrsEmpty)
            shouldThrow<PReader.PReaderException> {
                pr2.parse()
            }
        }
        "Неккоректный путь к файлу" {
            val pr = getPReader(agrsPathInvalid)
            shouldThrow<PReader.PReaderException> {
                pr.parse()
            }
        }
    }
}