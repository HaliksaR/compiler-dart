/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.paramsreader

class PReader(private val args: Array<String>) {

    class PReaderException(override val message: String) : Exception(message)

    private companion object {
        const val REGEX_PATH_DUMP = "(([^\\s]+)|default)"

        const val REGEX_DUMP_TOKENS = "--dump-tokens|-dt"
        const val REGEX_DUMP_TOKENS_NOT_COMMENTS = "(--dump-tokens|-dt)=(not-comments|nc)"
        const val REGEX_DUMP_TOKENS_TO_FILE = "(--dump-tokens|-dt)=$REGEX_PATH_DUMP"

        const val REGEX_DUMP_AST = "--dump-ast|-dast"
        const val REGEX_DUMP_AST_TO_FILE = "(--dump-ast|-dast)=$REGEX_PATH_DUMP"

        const val REGEX_DUMP_ID_TABLE = "--dump-id-table|-dt"
        const val REGEX_DUMP_ID_TABLE_TO_FILE = "(--dump-id-table|-dit)=$REGEX_PATH_DUMP"

        const val REGEX_DUMP_SEMA = "--dump-sema|-ds"
        const val REGEX_DUMP_SEMA_TO_FILE = "(--dump-sema|-ds)=$REGEX_PATH_DUMP"

        const val REGEX_DUMP_ASM = "--dump-asm|-dasm"
        const val REGEX_DUMP_ASM_TO_FILE = "(--dump-asm|-dasm)=$REGEX_PATH_DUMP"

        const val REGEX_PATH = "^(?!--)(/?[\\w-]+)+(.[a-zA-Z]+?)\$"
    }

    private var paramsLexer = mutableListOf<Param>()
    fun getParamsLexer(): List<Param> = paramsLexer.toList()

    private var paramsTable = mutableListOf<Param>()
    fun getParamsTable(): List<Param> = paramsTable.toList()

    private var paramsAST = mutableListOf<Param>()
    fun getParamsAST(): List<Param> = paramsAST.toList()

    private var paramsASM = mutableListOf<Param>()
    fun getParamsASM(): List<Param> = paramsASM.toList()

    private var paramsSema = mutableListOf<Param>()
    fun getParamsSema(): List<Param> = paramsSema.toList()

    private var pathSource: String? = null

    @Throws(PReaderException::class)
    fun getSourcePath(): String = pathSource ?: throw PReaderException("Source Path not found")

    private var pathOut: String? = null

    @Throws(PReaderException::class)
    fun getOutPath(): String = pathOut ?: throw PReaderException("Out Path not found")

    @Throws(PReaderException::class)
    fun parse() {
        if (args.isNullOrEmpty()) {
            throw PReaderException("Args not found")
        }

        args.forEach(::parseArg)

        if (pathSource == null) {
            throw PReaderException("File not found")
        }
    }

    private fun parseArg(arg: String) {
        when {
            arg.matches(REGEX_DUMP_TOKENS.toRegex()) -> {
                paramsLexer.add(Param(Params.DUMP_TOKENS))
            }
            arg.matches(REGEX_DUMP_TOKENS_NOT_COMMENTS.toRegex()) -> {
                paramsLexer.add(Param(Params.DUMP_TOKENS_NOT_COMMENTS))
            }
            arg.matches(REGEX_DUMP_TOKENS_TO_FILE.toRegex()) -> {
                val index = arg.indexOf('=')
                paramsLexer.add(Param(Params.DUMP_TOKENS_TO_FILE, arg.substring(index + 1)))
            }

            arg.matches(REGEX_DUMP_AST.toRegex()) -> {
                paramsAST.add(Param(Params.DUMP_AST))
            }
            arg.matches(REGEX_DUMP_AST_TO_FILE.toRegex()) -> {
                val index = arg.indexOf('=')
                paramsAST.add(Param(Params.DUMP_AST_TO_FILE, arg.substring(index + 1)))
            }

            arg.matches(REGEX_DUMP_ID_TABLE.toRegex()) -> {
                paramsTable.add(Param(Params.DUMP_ID_TABLE))
            }
            arg.matches(REGEX_DUMP_ID_TABLE_TO_FILE.toRegex()) -> {
                val index = arg.indexOf('=')
                paramsTable.add(Param(Params.DUMP_ID_TABLE_TO_FILE, arg.substring(index + 1)))
            }

            arg.matches(REGEX_DUMP_SEMA.toRegex()) -> {
                paramsSema.add(Param(Params.DUMP_SEMA))
            }
            arg.matches(REGEX_DUMP_SEMA_TO_FILE.toRegex()) -> {
                val index = arg.indexOf('=')
                paramsSema.add(Param(Params.DUMP_SEMA_TO_FILE, arg.substring(index + 1)))
            }

            arg.matches(REGEX_DUMP_ASM.toRegex()) -> {
                paramsASM.add(Param(Params.DUMP_ASM))
            }
            arg.matches(REGEX_DUMP_ASM_TO_FILE.toRegex()) -> {
                val index = arg.indexOf('=')
                paramsASM.add(Param(Params.DUMP_ASM_TO_FILE, arg.substring(index + 1)))
            }

            arg.matches(REGEX_PATH.toRegex()) -> {
                if (pathSource == null) {
                    pathSource = REGEX_PATH.toRegex().find(arg)?.value
                    return
                }
                if (pathOut == null) {
                    pathOut = REGEX_PATH.toRegex().find(arg)?.value
                    return
                }
            }

            else -> throw PReaderException("Unknown Key $arg")
        }
    }
}

