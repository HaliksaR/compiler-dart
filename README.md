Options:
 - `--dump-tokens` или `-dt` — вывести результат работы лексического анализатора
 
 - `--dump-tokens=not-comments` или `-dt=nc` — вывести результат работы лексического анализатора без комментариев
 
 - `--dump-tokens=<file||default>` или `-dt=<file||default>` — вывести результат работы лексического анализатора в файл (папка dumps/`date`/lexer.txt)
 
 - `--dump-ast` или `-dast` — вывести AST
 
 - `--dump-id-table=<file||default>` или `-dit=<file||default>` — вывести AST в файл (`default` папка dumps/`date`/ast.txt)
 
 - `--dump-id-table` или `-dit` — вывести таблицу областей видимости и их значения
 
 - `--dump-id-table=<file||default>` или `-dit=<file||default>` — вывести таблицу областей видимости и их значения  в файл (`default` папка dumps/`date`/id_table.txt)
 
 - `--dump-asm` или `-dasm` — вывести ассемблер
 
Run 
 ```bash
      java -jar dart_compiler-<version>.jar <options> <source.dart> <out.s>
 ```
Example
```bash
    java -jar dart_compiler-<version>.jar --dump-ast=ast.txt --dump-sema --dump-id-table=table.txt app/src/main/resources/DevTestSema.dart app/src/main/resources/test.s
```

Versions
- Language : [Kotlin 1.4.0](https://github.com/JetBrains/kotlin)
- jvmTarget : 1.8 (dev run in openjdk-14.0.2)
- Build : [Gradle 6.6](https://gradle.org/) + FatJar : [shadow 6.0.0](https://github.com/johnrengelman/shadow)

Libraries used:
- DI : [Koin 2.1.6](https://github.com/InsertKoinIO/koin)
- Colored output : [Mordant 1.2.1](https://github.com/ajalt/mordant)
- Test : [Kotest 4.2.3](https://github.com/kotest/kotest)
