/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.lexer

import com.haliksar.devel.core.token.Token
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.koin.test.KoinTest
import java.io.FileReader

class LexerTests : StringSpec(), KoinTest {
    private lateinit var all: FileReader
    private lateinit var allError: FileReader

    init {
        beforeTest {
            all = FileReader("src/test/resources/DevTestAll.dart")
            allError = FileReader("src/test/resources/DevTestAllError.dart")
        }

        "Корректные данные" {
            val lexer = Lexer(all)
            val tokens = lexer.parse()
            tokens.none { it.type == Token.Type.Unknown } shouldBe true
        }

        "Неккорентные данные" {
            val lexer = Lexer(allError)
            val tokens = lexer.parse()
            tokens.none { it.type == Token.Type.Unknown } shouldBe false
        }

        afterTest {
            all.close()
            allError.close()
        }
    }
}