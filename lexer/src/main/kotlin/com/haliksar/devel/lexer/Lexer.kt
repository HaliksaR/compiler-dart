/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.lexer

import com.haliksar.devel.core.output.DumpErrors
import com.haliksar.devel.core.output.DumpPrint
import com.haliksar.devel.core.output.DumpToFile
import com.haliksar.devel.core.output.DumpToFile.Companion.DEFAULT
import com.haliksar.devel.core.output.DumpToFile.Companion.DUMP_FOLDER
import com.haliksar.devel.core.output.OutputTerm
import com.haliksar.devel.core.token.Positions
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.lexer.ext.*
import com.haliksar.devel.paramsreader.Param
import com.haliksar.devel.paramsreader.Params
import org.koin.core.KoinComponent
import java.io.File
import java.io.FileReader
import java.io.PushbackReader
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set

class Lexer(bufferedReader: FileReader) : DumpPrint, DumpToFile, DumpErrors, KoinComponent {

    companion object {
        const val EMPTY_STRING = ""
        const val EOF_FILE = 65535
        const val END_FILE = -1
    }

    override val errors: MutableMap<Token, String> = mutableMapOf()

    private val reader = PushbackReader(bufferedReader)

    private fun getAndSetPosition() = reader.get {
        buffer += it.toChar()
        positions.nextLine(it.toChar())
    }

    private fun skipSetPosition() = reader.skip { positions.nextLine(it.toChar()) }

    private var buffer = EMPTY_STRING

    private val positions by lazy {
        Positions().apply {
            start = Positions.Point()
            end = Positions.Point()
        }
    }

    private val tokens = mutableListOf<Token>()

    private val patternNumber: (Int) -> Boolean = {
        "0123456789".contains(it.toChar())
    }

    private val patternIdentifier: (Int) -> Boolean = {
        "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_".contains(it.toChar())
    }

    private fun tokenUnknown(): Token = Token(
        lexeme = buffer,
        type = Token.Type.Unknown,
        positions = positions.checkPoint()
    ).also { errors[it] = "Unknown token" }

    fun parse(): List<Token> {
        do {
            tokens.add(findToken())
            positions.update()
            buffer = EMPTY_STRING
        } while (tokens.last().type != Token.Type.End)
        reader.close()
        return tokens.toList()
    }

    private fun findToken(): Token {
        while (reader.peek().toChar().isWhitespace()) {
            skipSetPosition()
        }
        positions.update()
        val char = getAndSetPosition()
        return if (char == EOF_FILE || char == END_FILE) {
            Token(
                lexeme = buffer,
                type = Token.Type.End,
                positions = positions.checkPoint()
            )
        } else {
            findType()
        }
    }

    private fun findType(): Token = when (buffer) {
        "." -> isAlone()
        "\'", "\"" -> isString()
        "/" -> isOperationOrComment()
        "<", ">", "=", "+", "-", "*", "!", "&", "|" -> isOperation()
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> isNumber()
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "_" -> isIdentifier()
        else -> isAlone()
    }

    private fun isAlone(): Token {
        val type = when (buffer) {
            "(" -> Token.Type.LParen
            ")" -> Token.Type.RParen
            "[" -> Token.Type.LBox
            "]" -> Token.Type.RBox
            "{" -> Token.Type.LFigured
            "}" -> Token.Type.RFigured
            "," -> Token.Type.Comma
            ":" -> Token.Type.Colon
            ";" -> Token.Type.Semicolon
            "." -> Token.Type.Dot
            else -> Token.Type.Unknown
        }
        return if (type != Token.Type.Unknown) {
            Token(
                lexeme = buffer,
                positions = positions.checkPoint(),
                type = type
            )
        } else {
            tokenUnknown()
        }
    }

    private fun isIdentifier(): Token {
        while (patternIdentifier(reader.peek())) getAndSetPosition()
        return Token(
            lexeme = buffer,
            positions = positions.checkPoint(),
            type = when (buffer) {
                "return" -> Token.Type.KeywordReturn
                "if" -> Token.Type.KeywordIf
                "else" -> Token.Type.KeywordElse
                "for" -> Token.Type.KeywordFor
                "import" -> Token.Type.KeywordImport
                "List" -> Token.Type.TypeList
                "int" -> Token.Type.TypeInt
                "String" -> Token.Type.TypeString
                "char" -> Token.Type.TypeChar
                "bool" -> Token.Type.TypeBoolean
                "true" -> Token.Type.BooleanLiteralTrue
                "false" -> Token.Type.BooleanLiteralFalse
                "while" -> Token.Type.KeywordWhile
                "do" -> Token.Type.KeywordDoWhile
                "print" -> Token.Type.KeywordPrint
                "stdin",
                "readLineSync" -> Token.Type.KeywordScan
                else -> Token.Type.Identifier
            }
        )
    }

    private fun isNumber(): Token {
        while (patternNumber(reader.peek())) {
            getAndSetPosition()
        }
        return Token(
            lexeme = buffer,
            type = Token.Type.IntLiteral,
            positions = positions.checkPoint()
        )
    }

    private fun isOperation(): Token = when (reader.peek().toChar()) {
        '=', '+', '-', '&', '|' -> isComparison()
        else -> isOperationAlone()
    }

    private fun isOperationAlone(): Token {
        val type = when (buffer) {
            "=" -> Token.Type.OperatorAssignment
            "+" -> Token.Type.OperatorPlus
            "-" -> Token.Type.OperatorMinus
            "*" -> Token.Type.OperatorMultiply
            "/" -> Token.Type.OperatorDivision
            "<" -> Token.Type.LessThan
            ">" -> Token.Type.GreaterThan
            "!" -> Token.Type.OperatorExclamationMark
            else -> Token.Type.Unknown
        }
        return if (type != Token.Type.Unknown) {
            Token(
                lexeme = buffer,
                positions = positions.checkPoint(),
                type = type
            )
        } else {
            tokenUnknown()
        }
    }

    private fun isComparison(): Token {
        getAndSetPosition()
        val type = when (buffer) {
            "==" -> Token.Type.EqualTo
            "<=" -> Token.Type.LessThanOrEqualTo
            ">=" -> Token.Type.GreaterThanOrEqualTo
            "!=" -> Token.Type.NotEqualTo
            "&&" -> Token.Type.OperatorAnd
            "||" -> Token.Type.OperatorOr
            else -> Token.Type.Unknown
        }
        return if (type != Token.Type.Unknown) {
            Token(
                lexeme = buffer,
                positions = positions.checkPoint(),
                type = type
            )
        } else {
            tokenUnknown()
        }
    }

    private fun isOperationOrComment(): Token = when (reader.peek().toChar()) {
        '/' -> isSLCommit()
        '*' -> isMLCommit()
        else -> isOperation()
    }

    private fun isMLCommit(): Token {
        getAndSetPosition()
        while (reader.peek() != END_FILE) {
            getAndSetPosition()
            if (buffer.last() == '*')
                if (getAndSetPosition().toChar() == '/')
                    return Token(
                        lexeme = buffer
                            .substring(2 until buffer.length - 2)
                            .trim(),
                        positions = positions.checkPoint(),
                        type = Token.Type.Comment
                    )
        }
        return tokenUnknown()
    }

    private fun isSLCommit(): Token {
        getAndSetPosition()
        while (reader.peek().toChar() != '\n' && reader.peek() != END_FILE && reader.peek() != EOF_FILE) {
            getAndSetPosition()
        }
        return Token(
            lexeme = buffer.substring(2).trim(),
            positions = positions.checkPoint(),
            type = Token.Type.Comment
        )
    }

    private fun isString(): Token {
        while (reader.peek() != -1) {
            getAndSetPosition()
            if (buffer.last() == buffer.first())
                return Token(
                    lexeme = buffer.substring(1 until buffer.length - 1),
                    positions = positions.checkPoint(),
                    type = Token.Type.StringLiteral
                )
        }
        return tokenUnknown()
    }

    override fun printDump(params: List<Param>, path: String) {
        val flag = when {
            params.find { it.value == Params.DUMP_TOKENS } != null -> 0
            params.find { it.value == Params.DUMP_TOKENS_NOT_COMMENTS } != null -> 1
            else -> 2
        }
        if (flag != 2)
            tokens.map {
                when {
                    flag == 0 && it.type == Token.Type.Comment -> println(OutputTerm().gray(it.dump(path)))
                    flag == 1 && it.type == Token.Type.Comment -> {
                        /* Nothing */
                    }
                    else -> println(OutputTerm().white(it.dump(path)))
                }
            }
        else tokens.map { if (it.type == Token.Type.Unknown) println(OutputTerm().red(it.dump(path))) }
    }

    override fun printDumpToFile(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_TOKENS_TO_FILE }
        if (param != null) {
            val path = when (val value = param.path) {
                DEFAULT -> {
                    super.printDumpToFile(params, path)
                    "${DUMP_FOLDER}/lexer.txt"
                }
                null -> throw NullPointerException("path is null")
                else -> value
            }

            File(path).printWriter().use { out ->
                tokens.forEach {
                    out.println(it.dump(path))
                }
            }
        }
    }

    override fun printErrors(path: String): Boolean {
        errors.forEach { (token, message) ->
            with(OutputTerm()) {
                println(red("E: $message ${token.dump(path)}"))
            }
        }
        return errors.isNotEmpty()
    }
}