/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.app

import com.haliksar.devel.asm.Asm
import com.haliksar.devel.asm.di.AsmModule
import com.haliksar.devel.ast.di.ASTModule
import com.haliksar.devel.ast.nodes.scopes.RootScopeNode
import com.haliksar.devel.core.di.CoreModule
import com.haliksar.devel.core.output.OutputTerm.catchPrintErrorOrValue
import com.haliksar.devel.core.output.OutputTerm.runWrapper
import com.haliksar.devel.core.output.OutputTermModule
import com.haliksar.devel.core.output.ResourceReader
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.idtable.IDTable
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.idtable.di.IDTableModule
import com.haliksar.devel.lexer.Lexer
import com.haliksar.devel.lexer.di.LexerModule
import com.haliksar.devel.paramsreader.PReader
import com.haliksar.devel.paramsreader.di.PReaderModule
import com.haliksar.devel.parser.Parser
import com.haliksar.devel.parser.di.ParserModule
import com.haliksar.devel.semantic.Semantic
import com.haliksar.devel.semantic.di.SemanticModule
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.get
import org.koin.core.parameter.parametersOf
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.FileReader
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    Application.start()

    val RR = Application.getResourceReader()

    val pReader: PReader = catchPrintErrorOrValue(
        func = { Application.getPReader(args).also { it.parse() } }
    )

    val source = File(pReader.getSourcePath())

    val inputStream: FileReader = catchPrintErrorOrValue(
        func = { Application.readFile(source.absolutePath) }
    )

    val lexer = Application.getLexer(inputStream)

    val tokens: List<Token> = runWrapper(
        startMessage = RR.getResource("lexer_start_message"),
        loadMessage = RR.getResource("lexer_load_message"),
        endMessage = RR.getResource("lexer_end_message"),
        task = lexer::parse,
        dumper = lexer,
        params = pReader.getParamsLexer(),
        fileName = source.name,
        onErrors = Application::exitProcess
    )

    val parser = Application.getParser(tokens)

    val tree: RootScopeNode = runWrapper(
        startMessage = RR.getResource("parser_start_message"),
        loadMessage = RR.getResource("parser_load_message"),
        endMessage = RR.getResource("parser_end_message"),
        task = {
            parser.parse()
            parser.tree
        },
        dumper = parser,
        params = pReader.getParamsAST(),
        fileName = source.name,
        onErrors = Application::exitProcess
    )

    val idTable = Application.getIDTable()

    val table: Map<String, List<Identifier>> = runWrapper(
        startMessage = RR.getResource("id_table_start_message"),
        loadMessage = RR.getResource("id_table_load_message"),
        endMessage = RR.getResource("id_table_end_message"),
        task = {
            idTable.buildTable(tree)
            idTable.getTable()
        },
        dumper = idTable,
        params = pReader.getParamsTable(),
        fileName = source.name,
        onErrors = Application::exitProcess
    )

    val semantic = Application.getSemantic(table)

    runWrapper(
        startMessage = RR.getResource("sema_start_message"),
        loadMessage = RR.getResource("sema_load_message"),
        endMessage = RR.getResource("sema_end_message"),
        task = { semantic.analyze(tree) },
        dumper = semantic,
        params = pReader.getParamsSema(),
        fileName = source.name,
        onErrors = Application::exitProcess
    )

    val out = File(pReader.getOutPath())

    val outputStream: FileOutputStream = catchPrintErrorOrValue(
        func = { FileOutputStream(out) }
    )

    val asm = Application.getAsm(table, outputStream)

    runWrapper(
        startMessage = RR.getResource("asm_start_message"),
        loadMessage = RR.getResource("asm_load_message"),
        endMessage = RR.getResource("asm_end_message"),
        task = { asm.build(tree) },
        dumper = asm,
        params = pReader.getParamsASM(),
        fileName = source.name,
        onErrors = Application::exitProcess
    )

    Application.stop()
}

object Application : KoinComponent {

    private fun initKoin() {
        startKoin {
            modules(
                CoreModule,
                OutputTermModule,
                PReaderModule,
                LexerModule,
                IDTableModule,
                ASTModule,
                ParserModule,
                SemanticModule,
                AsmModule
            )
        }
    }

    fun getResourceReader(): ResourceReader = get()

    fun getPReader(args: Array<String>): PReader =
        get(parameters = { parametersOf(args) })

    @Throws(FileNotFoundException::class)
    fun readFile(path: String) = FileReader(path)

    fun getLexer(inputStream: FileReader): Lexer =
        get(parameters = { parametersOf(inputStream) })

    fun getIDTable(): IDTable = get()

    fun getParser(tokens: List<Token>): Parser =
        get(parameters = { parametersOf(tokens) })

    fun getSemantic(idTable: Map<String, List<Identifier>>): Semantic =
        get(parameters = { parametersOf(idTable) })

    fun getAsm(idTable: Map<String, List<Identifier>>, out: FileOutputStream): Asm =
        get(parameters = { parametersOf(idTable, out) })

    fun start() = initKoin()

    fun stop() = stopKoin()


    fun exitProcess() {
        stop()
        exitProcess(-1)
    }
}