/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.app

import com.github.ajalt.mordant.TermColors
import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.ast.xml.XMLVisitorImpl
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.core.token.TokenManager
import com.haliksar.devel.idtable.IDTable
import com.haliksar.devel.idtable.visitor.IDTableVisitorImpl
import com.haliksar.devel.lexer.Lexer
import com.haliksar.devel.parser.Parser
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import java.io.FileReader

class IDTableTests : StringSpec(), KoinTest {
    private lateinit var notDeclarationParentScope: BaseNode
    private lateinit var notDeclarationOtherScope: BaseNode
    private lateinit var notMultiplyDeclaration: BaseNode
    private lateinit var allError: BaseNode

    private val moduleTest = module {
        factory { IDTable() }
        factory { IDTableVisitorImpl() }
        factory { XMLVisitorImpl() }
        single { TermColors() }
        single { (tokens: List<Token>) -> TokenManager(tokens) }
    }

    init {
        beforeTest {
            startKoin {
                modules(moduleTest)
            }
        }

        "Не инициализирована в родительском скопе" {
            notDeclarationParentScope = initAst("src/test/resources/notDeclarationParentScope.dart")
            val table = Application.getIDTable()
            table.buildTable(notDeclarationParentScope)
            table.errors.size shouldBe 3
        }

        "Не инициализирована в других скопах" {
            notDeclarationOtherScope = initAst("src/test/resources/notDeclarationOtherScope.dart")
            val table = Application.getIDTable()
            table.buildTable(notDeclarationOtherScope)
            table.errors.size shouldBe 3
        }

        "Множественная инициализация " {
            notMultiplyDeclaration = initAst("src/test/resources/notMultiplyDeclaration.dart")
            val table = Application.getIDTable()
            table.buildTable(notMultiplyDeclaration)
            table.errors.size shouldBe 16
        }

        "Все в кучке" {
            allError = initAst("src/test/resources/allError.dart")
            val table = Application.getIDTable()
            table.buildTable(allError)
            table.errors.size shouldBe 6
        }
        afterTest {
            stopKoin()
        }
    }

    private fun initAst(filePath: String): BaseNode {
        val file = FileReader(filePath)
        val lexer = Lexer(file)
        val tokens = lexer.parse()
        val parser = Parser(tokens)
        parser.parse()
        return parser.tree
    }
}