/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.core.output

import com.github.ajalt.mordant.TermColors
import com.haliksar.devel.paramsreader.Param
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.dsl.module
import kotlin.system.exitProcess

val OutputTermModule = module {
    single { TermColors() }
}

object OutputTerm : KoinComponent, Dumper {

    private val tc by inject<TermColors>()

    operator fun invoke() = tc

    fun <DATA> runWrapper(
        startMessage: String,
        loadMessage: String,
        task: () -> DATA,
        endMessage: String,
        dumper: Any,
        params: List<Param>,
        fileName: String,
        onErrors: () -> Unit = {},
        onWarnings: () -> Unit = {}
    ): DATA {
        println(this().reset(startMessage))
        println(this().blue(loadMessage))
        return catchPrintErrorOrValue(task, onErrors).also {
            var flagErrors = false
            if (dumper is DumpPrint) {
                dumpPrint(dumper, params, fileName)
            }
            if (dumper is DumpToFile) {
                dumpToFile(dumper, params, fileName)
            }
            if (dumper is DumpErrors && dumpErrors(dumper, fileName)) {
                flagErrors = true
                onErrors.invoke()
            }
            if (dumper is DumpWarnings && dumpWarnings(dumper, fileName)) {
                onWarnings.invoke()
            }
            if (!flagErrors) {
                println(this().green(endMessage))
            }
        }
    }

    inline fun <DATA> catchPrintErrorOrValue(func: () -> DATA, onErrors: () -> Unit = {}): DATA =
        try {
            func()
        } catch (exception: Throwable) {
            throw exception
            exception.message?.let { println(this().red(it)) }
            onErrors()
            exitProcess(-1)
        }
}
