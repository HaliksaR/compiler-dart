/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.core.ext

fun <K, V> Map<K, V>.asReversed(): Map<K, V> {
    val keys = this.keys
    val mutableMap = mutableMapOf<K, V>()
    for (i in this.size - 1 downTo 0) {
        mutableMap[keys.elementAt(i)] = this[keys.elementAt(i)] ?: throw NullPointerException()
    }
    return mutableMap.toMap()
}

fun <K, V> Map<K, List<V>>.asListElementReversed(): Map<K, List<V>> {
    val mutableMap = mutableMapOf<K, List<V>>()
    this.forEach { (key, value) ->
        mutableMap[key] = value.asReversed()

    }
    return mutableMap.toMap()
}