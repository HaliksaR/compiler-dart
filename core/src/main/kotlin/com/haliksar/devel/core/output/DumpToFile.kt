/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.core.output

import com.haliksar.devel.paramsreader.Param
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

interface DumpToFile {
    companion object {
        private const val DUMP_DIR = "dumps"
        private val date = SimpleDateFormat("dd_mm_yyyy_hh_mm_ss").format(Calendar.getInstance().time)
        val DUMP_FOLDER = "dumps/$date"
        const val DEFAULT = "default"
    }

    fun printDumpToFile(params: List<Param>, path: String) {
        val dir = File(DUMP_DIR)
        if (!dir.exists()) {
            dir.mkdir()
        }
        val dirDate = File(DUMP_FOLDER)
        if (!dirDate.exists()) {
            dirDate.mkdir()
        }
    }
}