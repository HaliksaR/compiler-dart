/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.idtable.visitor

import com.haliksar.devel.ast.nodes.RootSubjectNode
import com.haliksar.devel.ast.nodes.SubjectNode
import com.haliksar.devel.ast.nodes.call.CallArrayNode
import com.haliksar.devel.ast.nodes.call.CallFuncNode
import com.haliksar.devel.ast.nodes.call.CallVariableNode
import com.haliksar.devel.ast.nodes.decl.*
import com.haliksar.devel.ast.nodes.expr.*
import com.haliksar.devel.ast.nodes.scopes.*
import com.haliksar.devel.ast.nodes.structures.*
import com.haliksar.devel.ast.visitor.Visitor
import com.haliksar.devel.ast.visitor.forEachVisit
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.idtable.Identifier
import com.haliksar.devel.idtable.Scope
import com.haliksar.devel.idtable.add

class IDTableVisitorImpl : Visitor {

    private val table = mutableMapOf<String, MutableList<Identifier>>()
    fun getTable(): Map<String, List<Identifier>> = table

    private val scope = Scope()

    override fun visit(node: ImportNode) = Unit

    override fun visit(node: SubjectNode) {
        node.statement?.accept(this)
    }

    override fun visit(node: DeclFuncNode) {
        table.add(
            Identifier.DeclFunc(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        scope.add(node) {
            node.scope.accept(this)
        }
    }

    override fun visit(node: DeclVariableNode) {
        table.add(
            Identifier.DeclVariable(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        node.expression?.accept(this)
    }

    override fun visit(node: DeclVariablesNode) {
        node.getVariables().forEachVisit(this)
    }

    override fun visit(node: DeclArraysNode) {
        node.getVariables().forEachVisit(this)
    }

    override fun visit(node: DeclArrayNode) {
        table.add(
            Identifier.DeclArray(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        node.expression?.accept(this)
    }

    override fun visit(node: ParamsFuncNode) {
        node.getParams().forEachVisit(this)
    }

    override fun visit(node: ParamFuncNode) {
        table.add(
            Identifier.DeclVariable(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
    }

    override fun visit(node: ParamArrayFuncNode) {
        table.add(
            Identifier.DeclArray(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
    }

    override fun visit(node: CallFuncNode) {
        table.add(
            Identifier.CallFunc(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        node.getParams().forEachVisit(this)
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: CallVariableNode) {
        when (node.identifier.lexeme) {
            "length" -> {
                node.type = Token.Type.TypeInt
            }
        }
        table.add(
            Identifier.CallVariable(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: CallArrayNode) {
        table.add(
            Identifier.CallArray(
                id = node.identifier,
                node = node
            ),
            scope.depth
        )
        node.expressionArray.accept(this)
        node.expression?.accept(this)
        node.field?.accept(this)
    }

    override fun visit(node: ExprArithNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: IfNode) {
        scope.add(node) {
            node.expression.accept(this)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
        node.tree?.accept(this)
        node.elseBlock?.let {
            scope.add(it) {
                it.accept(this)
            }
        }
    }

    override fun visit(node: ForNode) {
        scope.add(node) {
            node.expressions.forEach { it?.accept(this) }
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: WhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: DoWhileNode) {
        scope.add(node) {
            node.expression.accept(this)
            scope.add(node.block) {
                node.block.accept(this)
            }
        }
    }

    override fun visit(node: ReturnNode) {
        node.expression?.accept(this)
    }

    override fun visit(node: ExprRelNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: ExprEqNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: FactorNode) {
        node.expression?.accept(this)
    }

    override fun visit(node: TermNode) {
        node.getExpressions().forEachVisit(this)
    }

    override fun visit(node: ArrayNode) {
        node.getElements().forEachVisit(this)
    }

    override fun visit(node: EntryExprNode) {
        node.expression.accept(this)
    }

    override fun visit(node: RootScopeNode) {
        scope.add(node) {
            node.imports?.accept(this)
            node.getFields().forEachVisit(this)
        }
    }

    override fun visit(node: ImportsNode) {
        node.getImports().forEachVisit(this)
    }

    override fun visit(node: RootSubjectNode) {
        node.subject.accept(this)
    }

    override fun visit(node: ScopeFuncNode) {
        node.params?.accept(this)
        node.scope.accept(this)
    }

    override fun visit(node: ScopeNode) {
        scope.add(node) {
            node.getStatements().forEachVisit(this)
        }
    }

    override fun visit(node: ScopeFieldNode) {
        node.statement?.accept(this)
        node.returned?.accept(this)
    }

    override fun visit(node: ScopeIdentifierNode) {
        node.statement.accept(this)
    }

    override fun visit(node: ScopeExprNode) {
        node.expression.accept(this)
    }

    override fun visit(node: PrintNode) {
        node.expression.accept(this)
    }

    override fun visit(node: ScanNode) = Unit

}