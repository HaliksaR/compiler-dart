/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.idtable

import com.haliksar.devel.ast.nodes.base.BaseNode
import com.haliksar.devel.core.output.*
import com.haliksar.devel.core.output.DumpToFile.Companion.DEFAULT
import com.haliksar.devel.core.output.DumpToFile.Companion.DUMP_FOLDER
import com.haliksar.devel.core.token.Token
import com.haliksar.devel.idtable.visitor.IDTableVisitorImpl
import com.haliksar.devel.paramsreader.Param
import com.haliksar.devel.paramsreader.Params
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.File

class IDTable : DumpPrint, KoinComponent, DumpErrors, DumpWarnings, DumpToFile {

    override val errors: MutableMap<Token, String> = mutableMapOf()
    override val warnings: MutableMap<Token, String> = mutableMapOf()

    private val visitor: IDTableVisitorImpl by inject()

    fun getTable(): Map<String, List<Identifier>> = visitor.getTable()


    fun buildTable(tree: BaseNode) {
        tree.accept(visitor)
        validateTable(visitor.getTable())
    }

    private fun Map<String, List<Identifier>>.forEachNested(action: (Identifier, String) -> Unit) =
        forEach { (key, value) -> value.forEach { action(it, key) } }

    private fun validateTable(table: Map<String, List<Identifier>>) {
        table.forEachNested { variable, scope ->
            if (variable.id.lexeme != "print" &&
                variable.id.lexeme != "stdin" &&
                variable.id.lexeme != "length" &&
                variable.id.lexeme != "readLineSync"
            ) {
                when (variable) {
                    is Identifier.DeclFunc,
                    is Identifier.DeclVariable,
                    is Identifier.DeclArray -> findDeclAgain(variable.id, table, scope)

                    is Identifier.CallFunc,
                    is Identifier.CallVariable -> findDecl(variable.id, table, scope)
                    is Identifier.CallArray -> {
                        findDeclArray(variable.id, table, scope)
                    }
                }
            }
        }
    }

    private fun findDecl(id: Token, table: Map<String, List<Identifier>>, scope: String) {
        var find = findDeclInParentScope(scope, id.lexeme, table)
        if (!find) {
            find = findDeclInOtherScope(scope, id.lexeme, table)
        }
        if (!find) {
            errors[id] = "Неопределенное имя '${id.lexeme}'."
        }
    }

    private fun findDeclArray(id: Token, table: Map<String, List<Identifier>>, scope: String) {
        var find = findDeclArrayInParentScope(scope, id.lexeme, table)
        if (!find) {
            find = findDeclArrayInOtherScope(scope, id.lexeme, table)
        }
        if (!find) {
            errors[id] = "Неопределенное имя '${id.lexeme}'."
        }
    }

    private fun findDeclArrayInOtherScope(
        scope: String,
        lexeme: String,
        table: Map<String, List<Identifier>>
    ): Boolean {
        table.forEach { (otherScope, variables) ->
            if (scope.contains(otherScope)) {
                variables.forEach {
                    if (it.getId() == lexeme && it is Identifier.DeclArray) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun findDeclArrayInParentScope(
        scope: String,
        lexeme: String,
        table: Map<String, List<Identifier>>
    ): Boolean {
        table[scope]?.forEach {
            if (it.getId() == lexeme && it is Identifier.DeclArray) {
                return true
            }
        }
        return false
    }

    private fun findDeclInOtherScope(
        scope: String,
        lexeme: String,
        table: Map<String, List<Identifier>>
    ): Boolean {
        table.forEach { (otherScope, variables) ->
            if (scope.contains(otherScope)) {
                variables.forEach {
                    if (it.getId() == lexeme && it is Identifier.DeclMarker) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun findDeclInParentScope(
        scope: String,
        lexeme: String,
        table: Map<String, List<Identifier>>
    ): Boolean {
        table[scope]?.forEach {
            if (it.getId() == lexeme && it is Identifier.DeclMarker) {
                return true
            }
        }
        return false
    }

    private fun findDeclAgain(id: Token, table: Map<String, List<Identifier>>, scope: String) {
        var counter = 0
        table[scope]?.forEach {
            if (it.id.lexeme == id.lexeme && it is Identifier.DeclMarker) {
                counter++
            }
        }
        if (counter != 1) {
            errors[id] = "Это имя '${id.lexeme}' Уже существует."
        }
    }

    override fun printDump(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_ID_TABLE }
        if (param != null) {
            getTable().forEach {
                println(it.dump())
            }
        }
    }

    override fun printDumpToFile(params: List<Param>, path: String) {
        val param = params.find { it.value == Params.DUMP_ID_TABLE_TO_FILE }
        if (param != null) {
            val pathname = when (val value = param.path) {
                DEFAULT -> {
                    super.printDumpToFile(params, path)
                    "${DUMP_FOLDER}/id_table.txt"
                }
                null -> throw NullPointerException("path is null")
                else -> value
            }

            File(pathname).printWriter().use { out ->
                getTable().forEach {
                    out.println(it.dump())
                }
            }
        }
    }

    override fun printErrors(path: String): Boolean {
        errors.forEach { (token, message) ->
            with(OutputTerm()) {
                println(red("E: $message\n $path: (${token.positions.start.line}, ${token.positions.start.column})"))
            }
        }
        return errors.isNotEmpty()
    }

    private fun Map.Entry<String, List<Identifier>>.dump() =
        "SCOPE : $key {\n" + value.joinToString(",\n\t", prefix = "\t") + "\n}"

    override fun printWarnings(path: String): Boolean {
        warnings.forEach { (token, message) ->
            with(OutputTerm()) {
                println(yellow("W: $message\n $path: (${token.positions.start.line}, ${token.positions.start.column})"))
            }
        }
        return warnings.isNotEmpty()
    }
}

fun <T> MutableMap<String, MutableList<T>>.add(variable: T, scope: List<String>) {
    val scope = scope.joinToString(Scope.SEPARATOR)
    if (this[scope] == null) {
        this[scope] = mutableListOf()
    }
    this[scope]?.add(variable)
}

inline fun <reified ID : Identifier> Map<String, List<Identifier>>.findIdentifier(
    scope: List<String>,
    id: String
): ID? {
    var result = this[scope.joinToString(Scope.SEPARATOR)]?.find { it.getId() == id && it is Identifier.DeclMarker }
    if (result == null) {
        val sc = scope.toMutableList()
        repeat(scope.size) {
            result = this[sc.joinToString(Scope.SEPARATOR)]?.find { it.getId() == id && it is ID }
            if (result != null) return result as? ID
            sc.removeLast()
        }
    }
    return result as? ID
}