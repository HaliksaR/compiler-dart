/*
 * Copyright (c)  2020, halik
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package com.haliksar.devel.idtable

import com.haliksar.devel.ast.ext.HasScope

class Scope {
    val depth: MutableList<String> = mutableListOf()

    companion object {
        private const val ASCII_A = 65
        private const val ASCII_Z = 90
        const val SEPARATOR = ""
    }

    private var symbolAccumulator = ASCII_A

    private fun MutableList<String>.down(nameScope: String) {
        add(getSymbol(nameScope))
        symbolAccumulator++
    }

    private fun MutableList<String>.up(nameScope: String) {
        removeLast()
        symbolAccumulator--
    }

    private fun getSymbol(nameScope: String): String {
        var accu = symbolAccumulator
        var symbols = "_"
        do {
            val symbol = ASCII_A + accu.rem(ASCII_A)
            symbols += symbol.toChar()
            accu -= ASCII_Z - ASCII_A
        } while (accu in ASCII_A..ASCII_Z)
        symbols += nameScope
        return symbols
    }

    fun add(node: HasScope, visit: () -> Unit) =
        apply {
            depth.down(node.scopeName())
            visit()
            depth.up(node.scopeName())
        }
}